package org.pantuquero.practica1.gui;

import javax.swing.*;
import java.awt.event.*;

public class ConectarBD extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfServidor;
    private JTextField tfBD;
    private JTextField tfUsuario;
    private JPasswordField tfContrasena;

    public ConectarBD() {
        tfServidor.setText(Ventana.servidor);
        tfBD.setText(Ventana.bd);
        tfUsuario.setText(Ventana.usuario);
        tfContrasena.setText(Ventana.contrasena);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(tfServidor.getText().equals("") || tfBD.getText().equals("") || tfUsuario.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Rellene todos los campos", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onOK();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        Ventana.servidor = tfServidor.getText();
        Ventana.bd = tfBD.getText();
        Ventana.usuario = tfUsuario.getText();
        Ventana.contrasena = String.valueOf(tfContrasena.getPassword());
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        ConectarBD dialog = new ConectarBD();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
