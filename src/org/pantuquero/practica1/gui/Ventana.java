package org.pantuquero.practica1.gui;

import com.toedter.calendar.JDateChooser;
import org.pantuquero.practica1.bd.Traductor;
import org.pantuquero.practica1.datos.Autobus;
import org.pantuquero.practica1.datos.Conductor;
import org.pantuquero.practica1.datos.Empresa;
import org.pantuquero.practica1.hilos.Refrescador;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;

/**
 * @author Álvaro Juan Basarte Bahón
 * @version 0.8
 */
public class Ventana {

    private JTabbedPane tpPestanas;
    private JPanel panel1;
    private JPanel pestanaEmpresas;
    private JPanel pesatanaAutobuses;
    private JPanel pestanaConductores;
    private JTextField tfNombreEmpresa;
    private JTextField tfDescripcionEmpresa;
    private JTextField tfCodigoEmpresa;
    private JTextField tfActivosEmpresa;
    private JList listaAutobusesEmpresa;
    private JList listaConductoresEmpresa;
    private JButton btNuevaEmpresa;
    private JButton btEliminarEmpresa;
    private JButton btCancelarEmpresa;
    private JButton btGuardarEmpresa;
    private JButton btModificarEmpresa;
    private JTextField tfBuscarEmpresa;
    private JList listaEmpresas;
    private JDateChooser dtFechaFundacionEmpresa;
    private JTextField tfNombreAutobus;
    private JTextField tfDescripcionAutobus;
    private JTextField tfMatriculaAutobus;
    private JTextField tfConsumoAutobus;
    private JList listaConductoresAutobus;
    private JList listaAutobuses;
    private JTextField tfBuscarAutobus;
    private JButton btNuevoAutobus;
    private JButton btModificarAutobus;
    private JButton btGuardarAutobus;
    private JButton btEliminarAutobus;
    private JButton btCancelarAutobus;
    private JDateChooser dtFechaCompraAutobus;
    private JTextField tfNombreConductor;
    private JTextField tfApellidoConductor;
    private JTextField tfJornadaConductor;
    private JTextField tfSalarioConductor;
    private JList listaConductores;
    private JButton btNuevoConductor;
    private JButton btModificarConductor;
    private JButton btGuardarConductor;
    private JButton btEliminarConductor;
    private JButton btCancelarConductor;
    private JTextField tfBuscarConductor;
    private JComboBox cbEmpresaAutobus;
    private JComboBox cbEmpresaConductor;
    private JComboBox cbAutobusConductor;
    private JDateChooser dtFechaNacimientoConductor;
    private JPanel pestanaBuscar;
    private JTextField tfEmpresasBusquedaAvanzada;
    private JList listaEmpresasBusquedaAvanzada;
    private JList listaAutobusesBusquedaAvanzada;
    private JList listaConductoresBusquedaAvanzada;
    private JTextField tfAutobusesBusquedaAvanzada;
    private JTextField tfConductoresBusquedaAvanzada;
    private JButton btExportarXML;
    private JButton btGuardarComoOpciones;
    private JCheckBox cbGuardadoManualOpciones;
    private JButton btGuardarCambiosOpciones;
    private JCheckBox cbGuardarEnRutaOpciones;
    private JTextField tfExaminarOpciones;
    private JButton btExaminarOpciones;
    private JButton btGuardarExaminarOpciones;
    private JPanel pestanaTodo;
    private JTable tablaEmpresas;
    private JTable tablaAutobuses;
    private JTable tablaConductores;
    private JButton btBuscarEmpresa;
    private JButton btBuscarAutobus;
    private JButton btBuscarConductor;
    private JTable tablaBuscarEmpresa;
    private JTable tablaBuscarAutobus;
    private JTable tablaBuscarConductor;
    private JScrollPane JScrollPane;

    private ArrayList<Empresa> empresas;
    private ArrayList<Autobus> autobuses;
    private ArrayList<Conductor> conductores;

    private int posicionEmpresa = 0;
    private int posicionAutobus = 0;
    private int posicionConductor = 0;

    private DefaultListModel<Empresa> modeloListaEmpresas;
    private DefaultListModel<Autobus> modeloliListaAutobuses;
    private DefaultListModel<Conductor> modeloListaConductores;

    private boolean nuevodato = true;

    boolean guardarDefinido = false;
    private String rutaGuardado = "";
    private Connection conexion;
    public static String servidor = "localhost";
    public static String bd = "pr2ad";
    public static String usuario = "root";
    public static String contrasena = "";

    private DefaultTableModel modeloTablaEmpresas;
    private DefaultTableModel modeloTablaAutobuses;
    private DefaultTableModel modeloTablaConductores;

    private Traductor traductor;

    private Refrescador refresher;
    private JMenuItem menuConfig;

    private String admin = "admin";
    private String pass = "admin";
    private JMenuItem menuAdmin;
    private boolean esAdmin;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Gestión de Transportistas");

        Ventana ventana = new Ventana();
        frame.setJMenuBar(ventana.Menu());

        frame.setContentPane(ventana.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Método principal
     */
    public Ventana(){
        this.empresas = new ArrayList<Empresa>();
        this.autobuses = new ArrayList<Autobus>();
        this.conductores = new ArrayList<Conductor>();

        Conectar();
        this.traductor = new Traductor(this.conexion);

        modeloListaEmpresas = new DefaultListModel<Empresa>();
        modeloliListaAutobuses = new DefaultListModel<Autobus>();
        modeloListaConductores = new DefaultListModel<Conductor>();

        //Añade las columnas a la pestaña que contiene las tablas con todos los datos
        modeloTablaEmpresas = new DefaultTableModel();
        modeloTablaAutobuses = new DefaultTableModel();
        modeloTablaConductores = new DefaultTableModel();

        this.esAdmin = false;

        this.modeloTablaEmpresas.addColumn("ID");
        this.modeloTablaEmpresas.addColumn("Nombe");
        this.modeloTablaEmpresas.addColumn("Descripcion");
        this.modeloTablaEmpresas.addColumn("Codigo");
        this.modeloTablaEmpresas.addColumn("Activos");
        this.modeloTablaEmpresas.addColumn("Fecha de fundacion");

        this.modeloTablaAutobuses.addColumn("ID");
        this.modeloTablaAutobuses.addColumn("Nombre");
        this.modeloTablaAutobuses.addColumn("Descripcion");
        this.modeloTablaAutobuses.addColumn("Matricula");
        this.modeloTablaAutobuses.addColumn("Consumo");
        this.modeloTablaAutobuses.addColumn("Fecha de compra");
        this.modeloTablaAutobuses.addColumn("ID empresa");

        this.modeloTablaConductores.addColumn("ID");
        this.modeloTablaConductores.addColumn("Nombre");
        this.modeloTablaConductores.addColumn("Apellido");
        this.modeloTablaConductores.addColumn("Fecha de nacimiento");
        this.modeloTablaConductores.addColumn("Jornada");
        this.modeloTablaConductores.addColumn("Salario");
        this.modeloTablaConductores.addColumn("ID empresa");
        this.modeloTablaConductores.addColumn("ID autobus");

        tablaEmpresas.setModel(modeloTablaEmpresas);
        tablaAutobuses.setModel(modeloTablaAutobuses);
        tablaConductores.setModel(modeloTablaConductores);

        tablaBuscarEmpresa.setModel(modeloTablaEmpresas);
        tablaBuscarAutobus.setModel(modeloTablaAutobuses);
        tablaBuscarConductor.setModel(modeloTablaConductores);

        cargarTablas();

        for(int i=0;i<=3;i++){
            modoNavegar(i);
        }
        refrescarDatos(0);

        refresher = new Refrescador(this, this.conexion);
        refresher.start();

        listaEmpresas.setModel(modeloListaEmpresas);
        listaAutobuses.setModel(modeloliListaAutobuses);
        listaConductores.setModel(modeloListaConductores);
        listaAutobusesEmpresa.setModel(modeloliListaAutobuses);
        listaConductoresEmpresa.setModel(modeloListaConductores);
        listaConductoresAutobus.setModel(modeloListaConductores);

        tpPestanas.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                cargarPestanaActual();
            }
        });

        btNuevaEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevodato = true;
                nuevo(tpPestanas.getSelectedIndex());
            }
        });

        btNuevoAutobus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevodato = true;
                nuevo(tpPestanas.getSelectedIndex());
            }
        });

        btNuevoConductor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevodato = true;
                nuevo(tpPestanas.getSelectedIndex());
            }
        });

        btGuardarEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    guardar(tpPestanas.getSelectedIndex());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        btGuardarAutobus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    guardar(tpPestanas.getSelectedIndex());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        btGuardarConductor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    guardar(tpPestanas.getSelectedIndex());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        listaEmpresas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(listaEmpresas.isEnabled())
                    moverseLista(tpPestanas.getSelectedIndex());
            }
        });

        listaAutobuses.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(listaAutobuses.isEnabled())
                    moverseLista(tpPestanas.getSelectedIndex());
            }
        });

        listaConductores.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(listaConductores.isEnabled())
                    moverseLista(tpPestanas.getSelectedIndex());
            }
        });

        btCancelarEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar(tpPestanas.getSelectedIndex());
            }
        });

        btCancelarAutobus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar(tpPestanas.getSelectedIndex());
            }
        });

        btCancelarConductor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar(tpPestanas.getSelectedIndex());
            }
        });

        btModificarEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevodato = false;
                modificar(tpPestanas.getSelectedIndex());
            }
        });

        btModificarAutobus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevodato = false;
                modificar(tpPestanas.getSelectedIndex());
            }
        });

        btModificarConductor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevodato = false;
                modificar(tpPestanas.getSelectedIndex());
            }
        });

        btEliminarEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminar(tpPestanas.getSelectedIndex());
            }
        });

        btEliminarAutobus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminar(tpPestanas.getSelectedIndex());
            }
        });

        btEliminarConductor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminar(tpPestanas.getSelectedIndex());
            }
        });

        tfBuscarEmpresa.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                buscar(tpPestanas.getSelectedIndex());
            }
        });

        tfBuscarAutobus.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                buscar(tpPestanas.getSelectedIndex());
            }
        });

        tfBuscarConductor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                buscar(tpPestanas.getSelectedIndex());
            }
        });

        btBuscarEmpresa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BuscarBD(0);
            }
        });

        btBuscarAutobus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BuscarBD(1);
            }
        });

        btBuscarConductor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BuscarBD(2);
            }
        });

    }

    /**
     * Método para buscar datos en la BD
     * @param indice índice del tipo de dato a buscar
     */
    public void BuscarBD(int indice){
        String texto;
        switch (indice){
            case 0:
                modeloTablaEmpresas.setNumRows(0);
                texto = tfEmpresasBusquedaAvanzada.getText();
                if(!texto.equals("")){
                    try {
                        Empresa empresa = traductor.buscarEmpresa(texto);
                        Object[] fila = new Object[]{
                                empresa.getId(),
                                empresa.getNombre(),
                                empresa.getDescripcion(),
                                empresa.getCodigo(),
                                empresa.getActivos(),
                                empresa.getFecha_fundacion()
                        };
                        modeloTablaEmpresas.addRow(fila);
                    } catch (SQLException e) {
                        System.out.println("Busqueda inconcluyente");
                    }
                }
                break;
            case 1:
                modeloTablaAutobuses.setNumRows(0);
                texto = tfAutobusesBusquedaAvanzada.getText();
                if(!texto.equals("")){
                    try {
                        Autobus autobus = traductor.buscarAutobus(texto);
                        Object[] fila = new Object[]{
                                autobus.getId(),
                                autobus.getNombre(),
                                autobus.getDescripcion(),
                                autobus.getMatricula(),
                                autobus.getConsumo(),
                                autobus.getFecha_compra(),
                                autobus.getEmpresa().getId()
                        };
                        modeloTablaAutobuses.addRow(fila);
                    } catch (SQLException e) {
                        System.out.println("Busqueda inconcluyente");
                    }
                }
                break;
            case 2:
                modeloTablaConductores.setNumRows(0);
                texto = tfConductoresBusquedaAvanzada.getText();
                if(!texto.equals("")){
                    try {
                        Conductor conductor = traductor.buscarConductor(texto);
                        Object[] fila = new Object[]{
                                conductor.getId(),
                                conductor.getNombre(),
                                conductor.getApellido(),
                                conductor.getFecha_nacimiento(),
                                conductor.getJornada(),
                                conductor.getSalario(),
                                conductor.getEmpresa().getId(),
                                conductor.getAutobus().getId()
                        };
                        modeloTablaConductores.addRow(fila);
                    } catch (SQLException e) {
                        System.out.println("Busqueda inconcluyente");
                    }
                }
                break;
        }
    }

    /**
     * Método para añadir la MenuBar a la ventana
     * @return
     */
    public JMenuBar Menu(){
        JMenuBar menuBar = new JMenuBar();

        JMenu menuConfiguracion = new JMenu("Configuracion");
        JMenuItem menuItemEditarConexion = new JMenuItem("Editar conexion");
        JMenuItem menuItemAdministrador = new JMenuItem("Modo administrador");
        menuItemEditarConexion.setEnabled(false);

        menuConfiguracion.add(menuItemAdministrador);
        menuConfiguracion.add(menuItemEditarConexion);
        menuBar.add(menuConfiguracion);

        this.menuAdmin = menuItemAdministrador;
        this.menuConfig = menuItemEditarConexion;

        menuItemAdministrador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ModoAdmin();
            }
        });

        menuItemEditarConexion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConectarBD dialogo = new ConectarBD();
                dialogo.setSize(195, 185);
                dialogo.setVisible(true);
                Conectar();
                cargarTablas();
            }
        });


        return menuBar;
    }

    /**
     * Metodo que te permite entrar al modo administrador
     */
    private void ModoAdmin(){
        DialogoAdministrador dialogo = new DialogoAdministrador();
        dialogo.setSize(242, 144);
        if(dialogo.mostrar() == false)
            return;

        if(this.admin.equals(dialogo.getAdministrador()) && this.pass.equals(dialogo.getContrasena())){
            JOptionPane.showMessageDialog(null, "Bienvenido, administrador");
            this.menuConfig.setEnabled(true);
            this.menuAdmin.setEnabled(false);
            this.esAdmin = true;
            modoNavegar(0);
            modoNavegar(1);
            modoNavegar(2);
        }else{
            JOptionPane.showMessageDialog(null, "Administrador o contraseña incorrectos");
        }
    }

    /**
     * Conecta con la base de datos
     */
    public void Conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            if(conexion!=null){
                this.conexion.close();
            }
            this.conexion = DriverManager.getConnection("jdbc:mysql://" + this.servidor + "/" + this.bd, this.usuario, this.contrasena);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            JOptionPane.showMessageDialog(null, "");
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "No se ha podido cargar el driver de la Base de Datos");
            e.printStackTrace();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se ha podido conectar con la Base de Datos");
            e.printStackTrace();
        }
    }

    /**
     * Carga las tablas de la base de datos en los ArrayList
     */
    public void cargarTablas(){
        try {
            if(!this.conexion.isClosed()){
                try{
                    empresas = this.traductor.getEmpresas();
                }catch (NullPointerException e){}
                try{
                    autobuses = this.traductor.getAutobuses();
                }catch (NullPointerException e){}
                try{
                    conductores = this.traductor.getConductores();
                }catch (NullPointerException e){}
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void vaciarListas(){
        this.empresas.clear();
        this.autobuses.clear();
        this.conductores.clear();
    }

    /**
     * Refresca los datos de la pestaña seleccionada en la posición 0
     */
    private void cargarPestanaActual(){
        int indice = tpPestanas.getSelectedIndex();

        switch(indice){
            case 0:
                posicionEmpresa = 0;
                tfBuscarEmpresa.setText("");
                refrescarDatos(indice);
                break;
            case 1:
                posicionAutobus = 0;
                tfBuscarAutobus.setText("");
                refrescarDatos(indice);
                break;
            case 2:
                posicionConductor = 0;
                tfBuscarConductor.setText("");
                refrescarDatos(indice);
                break;
            case 3:
                CargarTablas();
                break;
            case 4:
                tfEmpresasBusquedaAvanzada.setText("");
                tfAutobusesBusquedaAvanzada.setText("");
                tfConductoresBusquedaAvanzada.setText("");
                modeloTablaEmpresas.setNumRows(0);
                modeloTablaAutobuses.setNumRows(0);
                modeloTablaConductores.setNumRows(0);
                break;
            default:
                break;
        }
    }

    /**
     * Método que carga los datos de la base de datos en tablas para su consulta rápida
     */
    private void CargarTablas(){
        ArrayList <Empresa> lstEmpresas = new ArrayList<Empresa>();
        ArrayList <Autobus> lstAutobuses = new ArrayList<Autobus>();
        ArrayList <Conductor> lstConductores = new ArrayList<Conductor>();

        try {
            lstEmpresas = traductor.getEmpresas();
            lstAutobuses = traductor.getAutobuses();
            lstConductores = traductor.getConductores();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        modeloTablaEmpresas.setNumRows(0);
        for(Empresa empresa : lstEmpresas){
            Object[] fila = new Object[]{
                empresa.getId(),
                empresa.getNombre(),
                empresa.getDescripcion(),
                empresa.getCodigo(),
                empresa.getActivos(),
                empresa.getFecha_fundacion()
            };
            modeloTablaEmpresas.addRow(fila);
        }

        modeloTablaAutobuses.setNumRows(0);
        for(Autobus autobus : lstAutobuses){
            Object[] fila = new Object[]{
                    autobus.getId(),
                    autobus.getNombre(),
                    autobus.getDescripcion(),
                    autobus.getMatricula(),
                    autobus.getConsumo(),
                    autobus.getFecha_compra(),
                    autobus.getEmpresa().getId()
            };
            modeloTablaAutobuses.addRow(fila);
        }

        modeloTablaConductores.setNumRows(0);
        for(Conductor conductor : lstConductores){
            Object[] fila = new Object[]{
                    conductor.getId(),
                    conductor.getNombre(),
                    conductor.getApellido(),
                    conductor.getFecha_nacimiento(),
                    conductor.getJornada(),
                    conductor.getSalario(),
                    conductor.getEmpresa().getId(),
                    conductor.getAutobus().getId()
            };
            modeloTablaConductores.addRow(fila);
        }
    }

    /**
     * Método que carga el archivo introducido y devuelve el contenido en forma de objeto
     * @param nombre Nombre del archivo a buscar
     * @deprecated
     */
    private Object cargarArchivo(String nombre){
        ObjectInputStream deserializador = null;

        try {
            deserializador = new ObjectInputStream(new FileInputStream(nombre));
            Object objeto = deserializador.readObject();
            return objeto;
        } catch (FileNotFoundException fnfe) {
            JOptionPane.showMessageDialog(null, "Error al cargar el fichero", "Cargar", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException cnfe) {
            JOptionPane.showMessageDialog(null, "Error al cargar el fichero", "Cargar", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ioex) {
            JOptionPane.showMessageDialog(null, "Error al cargar el fichero", "Cargar", JOptionPane.ERROR_MESSAGE);
        }

        try {
            deserializador.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Método que guarda los archivos binarios de las ArrayList correspondientes
     */
    private void guardarArchivo(Object objeto, String nombre){
        ObjectOutputStream serializador = null;

        try {
            serializador = new ObjectOutputStream(new FileOutputStream(nombre));
            serializador.writeObject(objeto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que actualiza los datos de la pestaña introducida en la posición actual
     * @param pestana Posición de la pestaña a refrescar.
     */
    public void refrescarDatos(int pestana){
        switch (pestana){
            case 0:
                if(empresas.size()>0){
                    tfNombreEmpresa.setText(empresas.get(posicionEmpresa).getNombre());
                    tfDescripcionEmpresa.setText(empresas.get(posicionEmpresa).getDescripcion());
                    tfCodigoEmpresa.setText(String.valueOf(empresas.get(posicionEmpresa).getCodigo()));
                    tfActivosEmpresa.setText(String.valueOf(empresas.get(posicionEmpresa).getActivos()));
                    dtFechaFundacionEmpresa.setDate(empresas.get(posicionEmpresa).getFecha_fundacion());

                    if(empresas.get(posicionEmpresa).getAutobuses()!=null){
                        modeloliListaAutobuses.removeAllElements();
                        for(Autobus autobus : empresas.get(posicionEmpresa).getAutobuses()){
                            modeloliListaAutobuses.addElement(autobus);
                        }
                    }
                    if(empresas.get(posicionEmpresa).getConductores()!=null){
                        modeloListaConductores.removeAllElements();
                        for(Conductor conductor : empresas.get(posicionEmpresa).getConductores()){
                            modeloListaConductores.addElement(conductor);
                        }
                    }

                    modeloListaEmpresas.removeAllElements();
                    for(Empresa empresa : empresas){
                        modeloListaEmpresas.addElement(empresa);
                    }
                }else{
                    limpiarCajas(pestana);
                    modeloListaEmpresas.removeAllElements();
                    modeloliListaAutobuses.removeAllElements();
                    modeloListaConductores.removeAllElements();
                }
                break;
            case 1:
                if(autobuses.size()>0) {
                    tfNombreAutobus.setText(autobuses.get(posicionAutobus).getNombre());
                    tfDescripcionAutobus.setText(autobuses.get(posicionAutobus).getDescripcion());
                    tfMatriculaAutobus.setText(String.valueOf(autobuses.get(posicionAutobus).getMatricula()));
                    tfConsumoAutobus.setText(String.valueOf(autobuses.get(posicionAutobus).getConsumo()));
                    dtFechaCompraAutobus.setDate(autobuses.get(posicionAutobus).getFecha_compra());

                    cbEmpresaAutobus.removeAllItems();
                    if(autobuses.get(posicionAutobus).getEmpresa()!=null){
                        cbEmpresaAutobus.addItem(autobuses.get(posicionAutobus).getEmpresa());
                    }

                    if (autobuses.get(posicionAutobus).getConductores()!=null) {
                        modeloListaConductores.removeAllElements();
                        for (Conductor conductor : autobuses.get(posicionAutobus).getConductores()) {
                            modeloListaConductores.addElement(conductor);
                        }
                    }

                    modeloliListaAutobuses.removeAllElements();
                    for (Autobus autobus : autobuses) {
                        modeloliListaAutobuses.addElement(autobus);
                    }
                }else{
                    limpiarCajas(pestana);
                    modeloliListaAutobuses.removeAllElements();
                    modeloListaConductores.removeAllElements();
                }
                break;
            case 2:
                if(conductores.size()>0){
                    tfNombreConductor.setText(conductores.get(posicionConductor).getNombre());
                    tfApellidoConductor.setText(conductores.get(posicionConductor).getApellido());
                    dtFechaNacimientoConductor.setDate(conductores.get(posicionConductor).getFecha_nacimiento());
                    tfJornadaConductor.setText(String.valueOf(conductores.get(posicionConductor).getJornada()));
                    tfSalarioConductor.setText(String.valueOf(conductores.get(posicionConductor).getSalario()));

                    cbEmpresaConductor.removeAllItems();
                    if(conductores.get(posicionConductor).getEmpresa()!=null){
                        cbEmpresaConductor.addItem(conductores.get(posicionConductor).getEmpresa());
                    }
                    cbAutobusConductor.removeAllItems();
                    if(conductores.get(posicionConductor).getAutobus()!=null){
                        cbAutobusConductor.addItem(conductores.get(posicionConductor).getAutobus());
                    }

                    modeloListaConductores.removeAllElements();
                    for(Conductor conductor : conductores){
                        modeloListaConductores.addElement(conductor);
                    }
                }else{
                    limpiarCajas(pestana);
                    modeloListaConductores.removeAllElements();
                }
                break;
            default:
                break;
        }
    }

    /**
     * Método que habilita o deshabilita los componentes de la pestaña introducida para poder navegar
     * @param pestana Posicion de la pestaña por la que se desea navegar
     */
    private void modoNavegar(int pestana){
        switch (pestana){
            case 0:
                tpPestanas.setEnabled(true);
                tfNombreEmpresa.setEnabled(false);
                tfDescripcionEmpresa.setEnabled(false);
                tfCodigoEmpresa.setEnabled(false);
                tfActivosEmpresa.setEnabled(false);
                dtFechaFundacionEmpresa.setEnabled(false);
                btNuevaEmpresa.setEnabled(true);
                btCancelarEmpresa.setEnabled(false);
                btGuardarEmpresa.setEnabled(false);

                if(empresas.size()>0){
                    btModificarEmpresa.setEnabled(true);
                    if(this.esAdmin ==true){
                        btEliminarEmpresa.setEnabled(true);
                    }
                    listaEmpresas.setEnabled(true);
                    tfBuscarEmpresa.setEnabled(true);
                }else{
                    btModificarEmpresa.setEnabled(false);
                    btEliminarEmpresa.setEnabled(false);
                    listaEmpresas.setEnabled(false);
                    tfBuscarEmpresa.setEnabled(false);
                }
                break;
            case 1:
                tpPestanas.setEnabled(true);
                tfNombreAutobus.setEnabled(false);
                tfDescripcionAutobus.setEnabled(false);
                tfMatriculaAutobus.setEnabled(false);
                tfConsumoAutobus.setEnabled(false);
                dtFechaCompraAutobus.setEnabled(false);
                cbEmpresaAutobus.setEnabled(false);
                btNuevoAutobus.setEnabled(true);
                btCancelarAutobus.setEnabled(false);
                btGuardarAutobus.setEnabled(false);

                if(autobuses.size()>0){
                    btModificarAutobus.setEnabled(true);
                    if(this.esAdmin ==true)
                        btEliminarAutobus.setEnabled(true);
                    listaAutobuses.setEnabled(true);
                    tfBuscarAutobus.setEnabled(true);
                }else{
                    btModificarAutobus.setEnabled(false);
                    btEliminarAutobus.setEnabled(false);
                    listaAutobuses.setEnabled(false);
                    tfBuscarAutobus.setEnabled(false);
                }
                break;
            case 2:
                tpPestanas.setEnabled(true);
                tfNombreConductor.setEnabled(false);
                tfApellidoConductor.setEnabled(false);
                dtFechaNacimientoConductor.setEnabled(false);
                tfJornadaConductor.setEnabled(false);
                tfSalarioConductor.setEnabled(false);
                cbEmpresaConductor.setEnabled(false);
                cbAutobusConductor.setEnabled(false);
                btNuevoConductor.setEnabled(true);
                btCancelarConductor.setEnabled(false);
                btGuardarConductor.setEnabled(false);

                if(conductores.size()>0){
                    btModificarConductor.setEnabled(true);
                    if(this.esAdmin ==true)
                        btEliminarConductor.setEnabled(true);
                    listaConductores.setEnabled(true);
                    tfBuscarConductor.setEnabled(true);
                }else{
                    btModificarConductor.setEnabled(false);
                    btEliminarConductor.setEnabled(false);
                    listaConductores.setEnabled(false);
                    tfBuscarConductor.setEnabled(false);
                }
                break;
        }
    }

    /**
     * Metodo que habilita o deshabilita los componentes de la pestaña introducida para poder editar datos
     * @param pestana Posicion de la pestaña de la que se desea editar datos
     */
    private void modoEditar(int pestana){
        switch (pestana){
            case 0:
                tpPestanas.setEnabled(false);
                tfNombreEmpresa.setEnabled(true);
                tfDescripcionEmpresa.setEnabled(true);
                tfCodigoEmpresa.setEnabled(true);
                tfActivosEmpresa.setEnabled(true);
                dtFechaFundacionEmpresa.setEnabled(true);
                btNuevaEmpresa.setEnabled(false);
                btModificarEmpresa.setEnabled(false);
                btEliminarEmpresa.setEnabled(false);
                btGuardarEmpresa.setEnabled(true);
                btCancelarEmpresa.setEnabled(true);
                listaEmpresas.setEnabled(false);
                tfBuscarEmpresa.setEnabled(false);
                tfBuscarEmpresa.setText("");
                break;
            case 1:
                tpPestanas.setEnabled(false);
                tfNombreAutobus.setEnabled(true);
                tfDescripcionAutobus.setEnabled(true);
                tfMatriculaAutobus.setEnabled(true);
                tfConsumoAutobus.setEnabled(true);
                dtFechaCompraAutobus.setEnabled(true);
                cbEmpresaAutobus.setEnabled(true);
                btNuevoAutobus.setEnabled(false);
                btModificarAutobus.setEnabled(false);
                btEliminarAutobus.setEnabled(false);
                btGuardarAutobus.setEnabled(true);
                btCancelarAutobus.setEnabled(true);
                listaAutobuses.setEnabled(false);
                tfBuscarAutobus.setEnabled(false);
                tfBuscarAutobus.setText("");
                break;
            case 2:
                tpPestanas.setEnabled(false);
                tfNombreConductor.setEnabled(true);
                tfApellidoConductor.setEnabled(true);
                dtFechaNacimientoConductor.setEnabled(true);
                tfJornadaConductor.setEnabled(true);
                tfSalarioConductor.setEnabled(true);
                cbEmpresaConductor.setEnabled(true);
                cbAutobusConductor.setEnabled(true);
                btNuevoConductor.setEnabled(false);
                btModificarConductor.setEnabled(false);
                btEliminarConductor.setEnabled(false);
                btGuardarConductor.setEnabled(true);
                btCancelarConductor.setEnabled(true);
                listaConductores.setEnabled(false);
                tfBuscarConductor.setEnabled(false);
                tfBuscarConductor.setText("");
        }
    }

    /**
     * Método que te permite navegar por los datos mediante las listas dependiendo de la pestaña introducida
     * @param pestana Posicion de la pestaña por la cuyos datos se desea navegar
     */
    private void moverseLista(int pestana){
        switch (pestana){
            case 0:
                this.posicionEmpresa = listaEmpresas.getSelectedIndex();
                break;
            case 1:
                this.posicionAutobus = listaAutobuses.getSelectedIndex();
                break;
            case 2:
                this.posicionConductor = listaConductores.getSelectedIndex();
                break;
            default:
                break;
        }
        refrescarDatos(pestana);
    }

    /**
     * Método que vacía las cajas de la pestaña introducida
     * @param pestana Posición de la pestaña de la que se desa vaciar las cajas
     */
    private void limpiarCajas(int pestana){
        switch (pestana){
            case 0:
                tfNombreEmpresa.setText("");
                tfDescripcionEmpresa.setText("");
                tfCodigoEmpresa.setText("");
                tfActivosEmpresa.setText("");
                dtFechaFundacionEmpresa.setDate(null);

                modeloliListaAutobuses.removeAllElements();
                modeloListaConductores.removeAllElements();
                break;
            case 1:
                tfNombreAutobus.setText("");
                tfDescripcionAutobus.setText("");
                tfMatriculaAutobus.setText("");
                tfConsumoAutobus.setText("");
                dtFechaCompraAutobus.setDate(null);

                cbEmpresaAutobus.removeAllItems();

                modeloListaConductores.removeAllElements();
                break;
            case 2:
                tfNombreConductor.setText("");
                tfApellidoConductor.setText("");
                dtFechaNacimientoConductor.setDate(null);
                tfJornadaConductor.setText("");
                tfSalarioConductor.setText("");

                cbEmpresaConductor.removeAllItems();

                cbAutobusConductor.removeAllItems();
                break;
            default:
                break;
        }
    }

    /**
     * Método para introducir nuevos datos dependiendo de la pestaña introducida
     * @param pestana Posicion de la pestaña en la que se desea añadir los datos
     */
    private void nuevo(int pestana){
        switch (pestana){
            case 0:
                tfNombreEmpresa.setText("");
                tfDescripcionEmpresa.setText("");
                tfCodigoEmpresa.setText("");
                tfActivosEmpresa.setText("");
                dtFechaFundacionEmpresa.setDate(null);

                modeloliListaAutobuses.removeAllElements();
                modeloListaConductores.removeAllElements();

                modoEditar(pestana);
                break;
            case 1:
                tfNombreAutobus.setText("");
                tfDescripcionAutobus.setText("");
                tfMatriculaAutobus.setText("");
                tfConsumoAutobus.setText("");
                dtFechaCompraAutobus.setDate(null);

                cbEmpresaAutobus.removeAllItems();
                if(empresas!=null){
                    for(Empresa empresa : empresas){
                        cbEmpresaAutobus.addItem(empresa);
                    }
                }

                modeloListaConductores.removeAllElements();

                modoEditar(pestana);
                break;
            case 2:
                tfNombreConductor.setText("");
                tfApellidoConductor.setText("");
                dtFechaNacimientoConductor.setDate(null);
                tfJornadaConductor.setText("");
                tfSalarioConductor.setText("");

                cbEmpresaConductor.removeAllItems();
                if(empresas!=null){
                    for(Empresa empresa : empresas){
                        cbEmpresaConductor.addItem(empresa);
                    }
                }

                cbAutobusConductor.removeAllItems();
                if(autobuses!=null){
                    for(Autobus autobus : autobuses){
                        cbAutobusConductor.addItem(autobus);
                    }
                }
                modoEditar(pestana);
                break;
            default:
                break;
        }
    }

    /**
     * Método que guarda o modifica los datos introducidos correspondientes a la pestaña introducida en la memoria y en el archivo
     * @param pestana Posicion de la pestaña en la que se desa guardar los datos
     */
    private void guardar(int pestana) throws SQLException {
        switch (pestana){
            case 0:
                Empresa empresa;

                if(nuevodato == true){
                    empresa = new Empresa();
                }else{
                    empresa = empresas.get(posicionEmpresa);
                }

                if(tfNombreEmpresa.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca un nombre de la empresa", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    empresa.setNombre(tfNombreEmpresa.getText());
                }

                empresa.setDescripcion(tfDescripcionEmpresa.getText());

                if(tfCodigoEmpresa.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca el código de la empresa", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    try{
                        empresa.setCodigo(Integer.parseInt(tfCodigoEmpresa.getText()));
                    }catch (NumberFormatException e){
                        JOptionPane.showMessageDialog(null, "Introduzca un número en código (sin decimales)", "Tipo de dato incorrecto", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }

                if(tfActivosEmpresa.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca los activos de la empresa", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    try{
                        empresa.setActivos(Float.parseFloat(tfActivosEmpresa.getText()));
                    }catch (NumberFormatException e){
                        JOptionPane.showMessageDialog(null, "Introduzca un número en activos (con o sin decimales)", "Tipo de dato incorrecto", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }

                if(dtFechaFundacionEmpresa.getDate()!=null){
                    empresa.setFecha_fundacion(dtFechaFundacionEmpresa.getDate());
                }else{
                    JOptionPane.showMessageDialog(null, "Introduzca la fecha de fundación de la empresa", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }
                if(this.nuevodato==true){
                    try {
                        empresa.setId(this.traductor.nuevaEmpresa(empresa));
                    } catch (SQLException e) {
                        conexion.rollback();
                        e.printStackTrace();
                    }finally {
                        conexion.setAutoCommit(true);
                    }

                    this.empresas.add(empresa);
                }else if(nuevodato == false){
                    this.empresas.set(posicionEmpresa, empresa);

                    try {
                        this.traductor.modificarEmpresa(empresa);
                    } catch (SQLException e) {
                        conexion.rollback();
                        e.printStackTrace();
                    }finally {
                        conexion.setAutoCommit(true);
                    }
                }

                modoNavegar(pestana);
                refrescarDatos(pestana);
                break;
            case 1:
                Autobus autobus;

                if(nuevodato == true){
                    autobus = new Autobus();
                }else{
                    autobus = autobuses.get(posicionAutobus);
                }

                if(tfNombreAutobus.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca el nombre", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    autobus.setNombre(tfNombreAutobus.getText());
                }

                autobus.setDescripcion(tfDescripcionAutobus.getText());

                if(tfMatriculaAutobus.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca la matrícula", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    try{
                        autobus.setMatricula(Integer.parseInt(tfMatriculaAutobus.getText()));
                    }catch (NumberFormatException e){
                        JOptionPane.showMessageDialog(null, "Introduzca un número en la matrícula (sin decimales)", "Tipo de dato incorrecto", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }

                if(tfConsumoAutobus.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca el consumo medio", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    try{
                        autobus.setConsumo(Float.parseFloat(tfConsumoAutobus.getText()));
                    }catch (NumberFormatException e){
                        JOptionPane.showMessageDialog(null, "Introduzca un número en el consumo medio (con o sin decimales)", "Tipo de dato incorrecto", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }

                if(dtFechaCompraAutobus.getDate()!=null){
                    autobus.setFecha_compra(dtFechaCompraAutobus.getDate());
                }else{
                    JOptionPane.showMessageDialog(null, "Introduzca la fecha de compra", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }

                //Elimina el autobús de la empresa en la que se halle para reescribirlo con el nuevo dato
                if(cbEmpresaAutobus.getSelectedItem()!=null){
                    if(nuevodato == false && autobus.getEmpresa()!=null){
                        eliminarAutobusEmpresa(autobus);
                    }

                    empresas.get(cbEmpresaAutobus.getSelectedIndex()).addAutobus(autobus);
                    autobus.setEmpresa(empresas.get(cbEmpresaAutobus.getSelectedIndex()));
                }

                if(this.nuevodato == true){
                    this.autobuses.add(autobus);

                    try {
                        this.traductor.nuevoAutobus(autobus);
                    } catch (SQLException e) {
                        conexion.rollback();
                        e.printStackTrace();
                    }finally {
                        conexion.setAutoCommit(true);
                    }
                }else if(this.nuevodato == false){
                    this.autobuses.set(posicionAutobus, autobus);

                    try {
                        this.traductor.modificarAutobus(autobus);
                    } catch (SQLException e) {
                        conexion.rollback();
                        e.printStackTrace();
                    }finally {
                        conexion.setAutoCommit(true);
                    }
                }

                modoNavegar(pestana);
                refrescarDatos(pestana);
                break;
            case 2:
                Conductor conductor;

                if(nuevodato == true){
                    conductor = new Conductor();
                }else{
                    conductor = conductores.get(posicionConductor);
                }

                if(tfNombreConductor.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca el nombre", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    conductor.setNombre(tfNombreConductor.getText());
                }

                if(tfApellidoConductor.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca el apellido", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    conductor.setApellido(tfApellidoConductor.getText());
                }

                if(dtFechaNacimientoConductor.getDate()!=null){
                    conductor.setFecha_nacimiento(dtFechaNacimientoConductor.getDate());
                }else{
                    JOptionPane.showMessageDialog(null, "Introduzca la fecha de nacimiento", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }

                if(tfJornadaConductor.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca la jornada", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    try{
                        conductor.setJornada(Integer.parseInt(tfJornadaConductor.getText()));
                    }catch (NumberFormatException e){
                        JOptionPane.showMessageDialog(null, "Introduzca un numero en la jornada (sin decimales)", "Tipo de dato incorrecto", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }

                if(tfSalarioConductor.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Introduzca el salario", "Campo vacío", JOptionPane.WARNING_MESSAGE);
                    break;
                }else{
                    try{
                        conductor.setSalario(Float.parseFloat(tfSalarioConductor.getText()));
                    }catch (NumberFormatException e){
                        JOptionPane.showMessageDialog(null, "Introduzca un numero en el salario (con o sin decimales)", "Tipo de dato incorrecto", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }

                //Elimina el conductor de la empresa en que se halle para sobreescribirlo con el nuevo dato
                if(cbEmpresaConductor.getSelectedItem()!=null){
                    if(nuevodato == false && conductor.getEmpresa()!=null){
                        eliminarConductorEmpresa(conductor);
                    }
                    conductor.setEmpresa(empresas.get(cbEmpresaConductor.getSelectedIndex()));
                    empresas.get(cbEmpresaConductor.getSelectedIndex()).addConductor(conductor);
                }

                //Elimina el conductor del autobus en que se halle para sobreescribirlo con el nuevo dato
                if(cbAutobusConductor.getSelectedItem()!=null){
                    if(nuevodato == false && conductor.getEmpresa()!=null){
                        eliminarConductorAutobus(conductor);
                    }
                    conductor.setAutobus(autobuses.get(cbAutobusConductor.getSelectedIndex()));
                    autobuses.get(cbAutobusConductor.getSelectedIndex()).addConductor(conductor);
                }

                if(nuevodato == true){
                    this.conductores.add(conductor);

                    try {
                        this.traductor.nuevoConductor(conductor);
                    } catch (SQLException e) {
                        conexion.rollback();
                        e.printStackTrace();
                    }finally {
                        conexion.setAutoCommit(true);
                    }
                }else{
                    this.conductores.set(posicionConductor, conductor);

                    try {
                        this.traductor.modificarConductor(conductor);
                    } catch (SQLException e) {
                        conexion.rollback();
                        e.printStackTrace();
                    }finally {
                        conexion.setAutoCommit(true);
                    }
                }

                modoNavegar(pestana);
                refrescarDatos(pestana);
                break;
            default:
                break;
        }
    }

    /**
     * Método que cancela la edición o creación actual
     * @param pestana Posición de la pestaña a cancelar
     */
    private void cancelar(int pestana){
        limpiarCajas(pestana);
        modoNavegar(pestana);
        refrescarDatos(pestana);
    }

    /**
     * Metodo que permite modificar los datos presentados en la pestaña introducida
     * @param pestana Posicion de la pestaña a modificar
     */
    private void modificar(int pestana){
        this.nuevodato = false;
        switch (pestana){
            case 0:
                modoEditar(pestana);
                break;
            case 1:
                cbEmpresaAutobus.removeAllItems();
                if(empresas!=null){
                    for(Empresa empresa : empresas){
                        cbEmpresaAutobus.addItem(empresa);
                    }
                }
                modoEditar(pestana);
                break;
            case 2:
                cbEmpresaConductor.removeAllItems();
                if(empresas!=null){
                    for(Empresa empresa : empresas){
                        cbEmpresaConductor.addItem(empresa);
                    }
                }

                cbAutobusConductor.removeAllItems();
                if(empresas!=null){
                    for(Autobus autobus : autobuses){
                        cbAutobusConductor.addItem(autobus);
                    }
                }
                modoEditar(pestana);
                break;
        }
    }

    /**
     * Método que permite eliminar el dato seleccionado en la pestaña introducida
     * @param pestana Posicion de la pestaña a borrar el dato
     */
    private void eliminar(int pestana){
        int posicion = 0;
        switch (pestana){
            case 0:
                Empresa empresa = empresas.get(posicionEmpresa);

                if(autobuses.size()>0){
                    for(Autobus autobus : autobuses){
                        if(autobus.getEmpresa().equals(empresa)){
                            posicion = autobuses.indexOf(autobus);
                        }
                    }
                    autobuses.get(posicion).setEmpresa(null);
                }

                if(conductores.size()>0){
                    for(Conductor conductor : conductores){
                        if(conductor.getEmpresa().equals(empresa)){
                            posicion = conductores.indexOf(conductor);
                        }
                    }
                    conductores.get(posicion).setEmpresa(null);
                }

                empresas.remove(posicionEmpresa);
                if(posicionEmpresa!=0){
                    posicionEmpresa--;
                }

                try {
                    traductor.eliminarEmpresa(empresa);
                } catch (SQLException e) {
                    try {
                        conexion.rollback();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                }finally {
                    try {
                        conexion.setAutoCommit(true);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                refrescarDatos(pestana);
                modoNavegar(pestana);
                break;
            case 1:
                Autobus autobus = autobuses.get(posicionAutobus);

                if(empresas.size()>0){
                    eliminarAutobusEmpresa(autobus);
                }

                if(conductores.size()>0){
                    boolean salir = false;
                    for (Conductor conductor : conductores){
                        if(conductor.getAutobus()!=null){
                            if (conductor.getAutobus().equals(autobus)) {
                                posicion = conductores.indexOf(conductor);
                            }
                        }else{
                            salir = true;
                        }
                    }
                    if(salir==false){
                        conductores.get(posicion).setAutobus(null);
                    }
                }

                autobuses.remove(posicionAutobus);
                if(posicionAutobus!=0){
                    posicionAutobus--;
                }

                try {
                    this.traductor.eliminarAutobus(autobus);
                } catch (SQLException e) {
                    try {
                        conexion.rollback();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                }finally {
                    try {
                        conexion.setAutoCommit(true);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                refrescarDatos(pestana);
                modoNavegar(pestana);
                break;
            case 2:
                Conductor conductor = conductores.get(posicionConductor);

                if(empresas.size()>0){
                    eliminarConductorEmpresa(conductor);
                    //empresas.get(empresas.indexOf(conductor.getEmpresa())).removeConductor(conductor);
                }

                if(autobuses.size()>0){
                    eliminarConductorAutobus(conductor);
                    //autobuses.get(autobuses.indexOf(conductor.getAutobus())).removeConductor(conductor);
                }

                conductores.remove(posicionConductor);
                if(posicionConductor!=0){
                    posicionConductor--;
                }

                try {
                    this.traductor.eliminarConductor(conductor);
                } catch (SQLException e) {
                    try {
                        conexion.rollback();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                }finally {
                    try {
                        conexion.setAutoCommit(true);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                refrescarDatos(pestana);
                modoNavegar(pestana);
                break;
            default:
                break;
        }
    }

    /**
     * Elimina el autobus introducido de la empresa en que se halle
     * @param autobus Autobus que se desea eliminar
     */
    private void eliminarAutobusEmpresa(Autobus autobus){
        int posEmp = -1;
        int posBus = -1;
        boolean encontrado = false;

        //Recorre todas las empresas guardando la posición en que se halla
        for(Empresa empresa : empresas){
            posEmp++;
            //En cada empresa recorre los autobuses guardando la posición en que se halla
            for(Autobus bus : empresa.getAutobuses()){
                posBus++;
                //Si un dato único coincide se guarda la posición del autobús en la empresa que se halle y sale del bucle
                if(bus.getMatricula() == autobus.getMatricula()){
                    encontrado = true;
                    break;
                }
            }
            if(encontrado == false){
                posBus = -1;
            }else{
                break;
            }
        }

        //Con las posiciones encontradas borra el autobús de la empresa
        if(encontrado == true){
            empresas.get(posEmp).getAutobuses().remove(posBus);
        }
    }

    /**
     * Elimina el conductor introducido de la empresa en que se halle
     * @param conductor Conductor que se desea eliminar
     */
    private void eliminarConductorEmpresa(Conductor conductor){
        int posEmp = -1;
        int posCon = -1;
        boolean encontrado = false;
        for(Empresa empresa : empresas){
            posEmp++;
            for(Conductor con : empresa.getConductores()){
                posCon++;
                if(con.getNombre().equals(conductor.getNombre())){
                    encontrado = true;
                    break;
                }
            }
            if(encontrado == false){
                posCon = -1;
            }else{
                break;
            }
        }
        if(encontrado == true){
            empresas.get(posEmp).getConductores().remove(posCon);
        }
    }

    /**
     * Elimina el conductor introducido de los autobuses que conduce
     * @param conductor Conductor que se desea eliminar
     */
    private void eliminarConductorAutobus(Conductor conductor){
        int posBus = -1;
        int posCon = -1;
        boolean encontrado = false;
        for(Autobus autobus : autobuses){
            posBus++;
            for(Conductor con : autobus.getConductores()){
                posCon++;
                if(con.getNombre().equals(conductor.getNombre())){
                    encontrado = true;
                    break;
                }
            }
            if(encontrado == false){
                posCon = -1;
            }else{
                break;
            }
        }
        if(encontrado == true){
            autobuses.get(posBus).getConductores().remove(posCon);
        }
    }

    /**
     * Busca en las listas los nombres de la tf de buscar
     * @param pestana Posicion de la pestaña en la que se desea buscar
     */
    private void buscar(int pestana){
        switch (pestana){
            case 0:
                if(tfBuscarEmpresa.getText().equals("")){
                    modeloListaEmpresas.removeAllElements();
                    for(Empresa empresa : empresas){
                        modeloListaEmpresas.addElement(empresa);
                    }
                }else{
                    modeloListaEmpresas.removeAllElements();
                    for(Empresa empresa : empresas){
                        if(empresa.getNombre().contains(tfBuscarEmpresa.getText())){
                            modeloListaEmpresas.addElement(empresa);
                        }
                    }
                }
                break;
            case 1:
                if(tfBuscarAutobus.getText().equals("")){
                    modeloliListaAutobuses.removeAllElements();
                    for(Autobus autobus : autobuses){
                        modeloliListaAutobuses.addElement(autobus);
                    }
                }else{
                    modeloliListaAutobuses.removeAllElements();
                    for(Autobus autobus : autobuses){
                        if(autobus.getNombre().contains(tfBuscarAutobus.getText())){
                            modeloliListaAutobuses.addElement(autobus);
                        }
                    }
                }
                break;
            case 2:
                if(tfBuscarConductor.getText().equals("")){
                    modeloListaConductores.removeAllElements();
                    for(Conductor conductor : conductores){
                        modeloListaConductores.addElement(conductor);
                    }
                }else{
                    modeloListaConductores.removeAllElements();
                    for(Conductor conductor : conductores){
                        if(conductor.getNombre().contains(tfBuscarConductor.getText())){
                            modeloListaConductores.addElement(conductor);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * Método que permite buscar cualquier tipo de campo en cualquier dato
     * @param tipoDato Tipo de datos en los que se buscará la información especificada
     * @deprecated
     */
    private void busquedaAvanzada(int tipoDato){
        boolean encontrado = false;
        switch (tipoDato){
            case 0:
                if(empresas.size()>0){
                    if(tfEmpresasBusquedaAvanzada.getText().equals("")){
                        modeloListaEmpresas.removeAllElements();
                    }else{
                        modeloListaEmpresas.removeAllElements();
                        for(Empresa empresa : empresas){
                            if(empresa.getNombre().contains(tfEmpresasBusquedaAvanzada.getText())){
                                modeloListaEmpresas.addElement(empresa);
                                encontrado = true;
                            }
                            try{
                                if(encontrado == false && empresa.getCodigo() == Integer.parseInt(tfEmpresasBusquedaAvanzada.getText())){
                                    modeloListaEmpresas.addElement(empresa);
                                    encontrado = true;
                                }
                            }catch (NumberFormatException e){}
                            try{
                                if(encontrado == false && empresa.getActivos() == Float.parseFloat(tfEmpresasBusquedaAvanzada.getText())){
                                    modeloListaEmpresas.addElement(empresa);
                                    encontrado = true;
                                }
                            }catch (NumberFormatException e){}
                            SimpleDateFormat formato = new SimpleDateFormat("dd-MMM-yyyy");
                            String fecha = formato.format(empresa.getFecha_fundacion());
                            if(encontrado == false && fecha.contains(tfEmpresasBusquedaAvanzada.getText())){
                                modeloListaEmpresas.addElement(empresa);
                                encontrado = true;
                            }
                            if(encontrado == false && empresa.getAutobuses().size()>0){
                                for(Autobus autobus : empresa.getAutobuses()){
                                    if(autobus.getNombre().contains(tfEmpresasBusquedaAvanzada.getText())){
                                        modeloListaEmpresas.addElement(empresa);
                                        encontrado = true;
                                    }
                                }
                            }
                            if(encontrado == false && empresa.getConductores().size()>0){
                                for(Conductor conductor : empresa.getConductores()){
                                    if(conductor.getNombre().contains(tfEmpresasBusquedaAvanzada.getText())){
                                        modeloListaEmpresas.addElement(empresa);
                                        encontrado = true;
                                    }
                                }
                            }
                            encontrado = false;
                        }
                    }
                }
                break;
            case 1:
                if(autobuses.size()>0){
                    if(tfAutobusesBusquedaAvanzada.getText().equals("")){
                        modeloliListaAutobuses.removeAllElements();
                    }else{
                        modeloliListaAutobuses.removeAllElements();
                        for(Autobus autobus : autobuses){
                            if(autobus.getNombre().contains(tfAutobusesBusquedaAvanzada.getText())){
                                modeloliListaAutobuses.addElement(autobus);
                                encontrado = true;
                            }
                            try{
                                if(encontrado == false && autobus.getMatricula() == Integer.parseInt(tfAutobusesBusquedaAvanzada.getText())){
                                    modeloliListaAutobuses.addElement(autobus);
                                    encontrado = true;
                                }
                            }catch (NumberFormatException e){}
                            try{
                                if(encontrado == false && autobus.getConsumo() == Float.parseFloat(tfAutobusesBusquedaAvanzada.getText())){
                                    modeloliListaAutobuses.addElement(autobus);
                                    encontrado = true;
                                }
                            }catch (NumberFormatException e){}
                            SimpleDateFormat formato = new SimpleDateFormat("dd-MMM-yyyy");
                            String fecha = formato.format(autobus.getFecha_compra());
                            if(encontrado == false && fecha.contains(tfAutobusesBusquedaAvanzada.getText())){
                                modeloliListaAutobuses.addElement(autobus);
                                encontrado = true;
                            }
                            if(encontrado == false && autobus.getEmpresa().getNombre().contains(tfAutobusesBusquedaAvanzada.getText())){
                                modeloliListaAutobuses.addElement(autobus);
                                encontrado = true;
                            }

                            if(encontrado == false && autobus.getConductores().size()>0){
                                for(Conductor conductor : autobus.getConductores()){
                                    if(conductor.getNombre().contains(tfAutobusesBusquedaAvanzada.getText())){
                                        modeloliListaAutobuses.addElement(autobus);
                                        encontrado = true;
                                    }
                                }
                            }
                            encontrado = false;
                        }
                    }
                }
                break;
            case 2:
                if(conductores.size()>0){
                    if(tfConductoresBusquedaAvanzada.getText().equals("")){
                        modeloListaConductores.removeAllElements();
                    }else{
                        modeloListaConductores.removeAllElements();
                        for(Conductor conductor : conductores){
                            if(conductor.getNombre().contains(tfConductoresBusquedaAvanzada.getText())){
                                modeloListaConductores.addElement(conductor);
                                encontrado = true;
                            }
                            if(conductor.getApellido().contains(tfConductoresBusquedaAvanzada.getText())){
                                modeloListaConductores.addElement(conductor);
                                encontrado = true;
                            }
                            SimpleDateFormat formato = new SimpleDateFormat("dd-MMM-yyyy");
                            String fecha = formato.format(conductor.getFecha_nacimiento());
                            if(encontrado == false && fecha.contains(tfConductoresBusquedaAvanzada.getText())){
                                modeloListaConductores.addElement(conductor);
                                encontrado = true;
                            }
                            try{
                                if(encontrado == false && conductor.getJornada() == Integer.parseInt(tfConductoresBusquedaAvanzada.getText())){
                                    modeloListaConductores.addElement(conductor);
                                    encontrado = true;
                                }
                            }catch (NumberFormatException e){}
                            try{
                                if(encontrado == false && conductor.getSalario() == Float.parseFloat(tfConductoresBusquedaAvanzada.getText())){
                                    modeloListaConductores.addElement(conductor);
                                    encontrado = true;
                                }
                            }catch (NumberFormatException e){}

                            if(encontrado == false && conductor.getEmpresa().getNombre().contains(tfConductoresBusquedaAvanzada.getText())){
                                modeloListaConductores.addElement(conductor);
                                encontrado = true;
                            }
                            if(encontrado == false && conductor.getAutobus().getNombre().contains(tfConductoresBusquedaAvanzada.getText())){
                                modeloListaConductores.addElement(conductor);
                                encontrado = true;
                            }

                            encontrado = false;
                        }
                    }
                }
                break;
        }
    }

    /**
     * Método que permite guardar los archivos de datos en la ruta buscada con el FileChooser
     */
    private void guardarComo(){
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        jFileChooser.setApproveButtonText("Guardar");

        if(jFileChooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION){
            return;
        }

        String ruta = jFileChooser.getSelectedFile().getAbsolutePath() + "\\";
        guardarArchivo(empresas, ruta + "empresas.dat");
        guardarArchivo(autobuses, ruta + "autobuses.dat");
        guardarArchivo(conductores, ruta + "conductores.dat");
    }

    /**
     * Método que recoge la ubicación seleccionada mediante el jFileChooser para guardar los archivos en ella
     */
    private void examinar(){
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        jFileChooser.setApproveButtonText("Seleccionar");

        if(jFileChooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION){
            return;
        }

        String ruta = jFileChooser.getSelectedFile().getAbsolutePath() + "\\";
        tfExaminarOpciones.setText(ruta);
        this.rutaGuardado = ruta;
        btGuardarExaminarOpciones.setEnabled(true);

        Properties configuracion = new Properties();
        configuracion.setProperty("guardarDefinido", String.valueOf(guardarDefinido));
        configuracion.setProperty("ruta", ruta);
        try {
            configuracion.store(new FileOutputStream("config.conf"), "Archivo de configuracion");
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}