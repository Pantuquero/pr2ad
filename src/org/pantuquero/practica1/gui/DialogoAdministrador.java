package org.pantuquero.practica1.gui;

import javax.swing.*;
import java.awt.event.*;

public class DialogoAdministrador extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfAdministrador;
    private JPasswordField tfContrasena;

    private String administrador;
    private String contrasena;
    private boolean opcion = false;

    public DialogoAdministrador() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        this.administrador = tfAdministrador.getText();
        this.contrasena = String.valueOf(tfContrasena.getPassword());
        this.opcion = true;
        setVisible(false);
    }

    private void onCancel() {
        this.opcion = false;
        setVisible(false);
    }

    public static void main(String[] args) {
        DialogoAdministrador dialog = new DialogoAdministrador();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public boolean mostrar(){
        setVisible(true);
        return opcion;
    }

    public String getAdministrador() {
        return administrador;
    }

    public void setAdministrador(String administrador) {
        this.administrador = administrador;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public boolean isOpcion() {
        return opcion;
    }

    public void setOpcion(boolean opcion) {
        this.opcion = opcion;
    }
}
