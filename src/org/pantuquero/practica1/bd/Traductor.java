package org.pantuquero.practica1.bd;

import org.pantuquero.practica1.datos.Autobus;
import org.pantuquero.practica1.datos.Conductor;
import org.pantuquero.practica1.datos.Empresa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Pantuquero on 15/12/2014.
 */
public class Traductor {
    private Connection conexion;

    public Traductor (Connection conexion){
        this.conexion = conexion;
    }

    public Empresa getEmpresa(int id) throws SQLException{
        Empresa empresa = new Empresa();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM empresas WHERE id = '"+id+"';");
        resultado = sentencia.executeQuery();

        resultado.first();

        empresa.setId(resultado.getInt(1));
        empresa.setNombre(resultado.getString(2));
        empresa.setDescripcion(resultado.getString(3));
        empresa.setCodigo(resultado.getInt(4));
        empresa.setActivos(resultado.getFloat(5));
        empresa.setFecha_fundacion(resultado.getDate(6));

        if (sentencia != null)
            sentencia.close();

        return empresa;
    }

    public Autobus getAutobus (int id) throws SQLException{
        Autobus autobus = new Autobus();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM autobuses WHERE id = '"+id+"';");
        resultado = sentencia.executeQuery();

        resultado.first();

        autobus.setId(resultado.getInt(1));
        autobus.setNombre(resultado.getString(2));
        autobus.setDescripcion(resultado.getString(3));
        autobus.setMatricula(resultado.getInt(4));
        autobus.setConsumo(resultado.getFloat(5));
        autobus.setFecha_compra(resultado.getDate(6));
        autobus.setEmpresa(getEmpresa(resultado.getInt(7)));

        if (sentencia != null)
            sentencia.close();

        return autobus;
    }

    public Conductor getConductor (int id) throws SQLException{
        Conductor conductor = new Conductor();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM conductores WHERE id = '"+id+"';");
        resultado = sentencia.executeQuery();

        resultado.first();

        conductor.setId(resultado.getInt(1));
        conductor.setNombre(resultado.getString(2));
        conductor.setApellido(resultado.getString(3));
        conductor.setFecha_nacimiento(resultado.getDate(4));
        conductor.setJornada(resultado.getInt(5));
        conductor.setSalario(resultado.getFloat(6));
        conductor.setEmpresa(getEmpresa(resultado.getInt(7)));
        conductor.setAutobus(getAutobus(resultado.getInt(8)));

        if (sentencia != null)
            sentencia.close();

        return conductor;
    }

    public ArrayList<Empresa> getEmpresas() throws SQLException {
        ArrayList<Empresa> listaEmpresas = new ArrayList<Empresa>();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM empresas");
        resultado = sentencia.executeQuery();

        while (resultado.next()) {
            Empresa empresa = new Empresa();

            empresa.setId(resultado.getInt(1));
            empresa.setNombre(resultado.getString(2));
            empresa.setDescripcion(resultado.getString(3));
            empresa.setCodigo(resultado.getInt(4));
            empresa.setActivos(resultado.getFloat(5));
            empresa.setFecha_fundacion(resultado.getDate(6));
            empresa.setAutobuses(getAutobusesEmpresa(empresa));
            empresa.setConductores(getConductoresDeX("empresa", empresa, null));

            listaEmpresas.add(empresa);
        }
        if (sentencia != null)
            sentencia.close();

        return listaEmpresas;
    }

    public ArrayList<Autobus> getAutobuses() throws SQLException{
        ArrayList<Autobus> listaBuses = new ArrayList<Autobus>();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM autobuses");
        resultado = sentencia.executeQuery();

        while (resultado.next()) {
            Autobus autobus = new Autobus();

            autobus.setId(resultado.getInt(1));
            autobus.setNombre(resultado.getString(2));
            autobus.setDescripcion(resultado.getString(3));
            autobus.setMatricula(resultado.getInt(4));
            autobus.setConsumo(resultado.getFloat(5));
            autobus.setFecha_compra(resultado.getDate(6));
            autobus.setEmpresa(getEmpresa(resultado.getInt(7)));
            autobus.setConductores(getConductoresDeX("autobus", null, autobus));
            listaBuses.add(autobus);
        }
        if (sentencia != null)
            sentencia.close();

        return listaBuses;
    }

    public ArrayList<Conductor> getConductores() throws SQLException{
        ArrayList<Conductor> listaConductores = new ArrayList<Conductor>();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM conductores");
        resultado = sentencia.executeQuery();

        while (resultado.next()) {
            Conductor conductor = new Conductor();

            conductor.setId(resultado.getInt(1));
            conductor.setNombre(resultado.getString(2));
            conductor.setApellido(resultado.getString(3));
            conductor.setFecha_nacimiento(resultado.getDate(4));
            conductor.setJornada(resultado.getInt(5));
            conductor.setSalario(resultado.getFloat(6));
            conductor.setEmpresa(getEmpresa(resultado.getInt(7)));
            conductor.setAutobus(getAutobus(resultado.getInt(8)));
            listaConductores.add(conductor);
        }
        if (sentencia != null)
            sentencia.close();

        return listaConductores;
    }

    public int nuevaEmpresa (Empresa empresa) throws SQLException{
        int id;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        java.sql.Date fechaSQL = new java.sql.Date(System.currentTimeMillis());

        conexion.setAutoCommit(false);

        fechaSQL = new java.sql.Date(empresa.getFecha_fundacion().getTime());

        sentencia = conexion.prepareStatement("INSERT INTO empresas (nombre, descripcion, codigo, activos, fecha_fundacion) " +
                "VALUES ('" + empresa.getNombre() + "','" + empresa.getDescripcion() + "','" + empresa.getCodigo() + "','" + empresa.getActivos() + "','" + fechaSQL + "');");
        sentencia.executeUpdate();
        conexion.commit();

        sentencia = conexion.prepareStatement("SELECT LAST_INSERT_ID()");
        resultado = sentencia.executeQuery();
        conexion.commit();
        resultado.first();
        id = resultado.getInt(1);

        if (sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);

        return id;
    }

    public int nuevoAutobus (Autobus autobus) throws SQLException{
        int id;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        java.sql.Date fechaSQL = new java.sql.Date(System.currentTimeMillis());

        conexion.setAutoCommit(false);

        fechaSQL = new java.sql.Date(autobus.getFecha_compra().getTime());

        sentencia = conexion.prepareStatement("INSERT INTO autobuses (nombre, descripcion, matricula, consumo, fecha_compra, id_empresa) " +
                "VALUES ('"+autobus.getNombre()+"','"+autobus.getDescripcion()+"','"+autobus.getMatricula()+"','"+autobus.getConsumo()+"','"+fechaSQL+"','"+autobus.getEmpresa().getId()+"');");
        sentencia.executeUpdate();
        conexion.commit();

        sentencia = conexion.prepareStatement("SELECT LAST_INSERT_ID()");
        resultado = sentencia.executeQuery();
        conexion.commit();
        resultado.first();
        id = resultado.getInt(1);

        if (sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);

        return id;
    }

    public int nuevoConductor (Conductor conductor) throws SQLException{
        int id;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        java.sql.Date fechaSQL = new java.sql.Date(System.currentTimeMillis());

        conexion.setAutoCommit(false);

        fechaSQL = new java.sql.Date(conductor.getFecha_nacimiento().getTime());

        sentencia = conexion.prepareStatement("INSERT INTO conductores (nombre, apellido, fecha_nacimiento, jornada, salario, id_empresa, id_autobus) " +
                "VALUES ('"+conductor.getNombre()+"','"+conductor.getApellido()+"','"+fechaSQL+"','"+conductor.getJornada()+"'," +
                "'"+conductor.getSalario()+"','"+conductor.getEmpresa().getId()+"','"+conductor.getAutobus().getId()+"');");
        sentencia.executeUpdate();
        conexion.commit();

        sentencia = conexion.prepareStatement("SELECT LAST_INSERT_ID()");
        resultado = sentencia.executeQuery();
        conexion.commit();
        resultado.first();
        id = resultado.getInt(1);

        if (sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);

        return id;
    }

    public void modificarEmpresa(Empresa empresa) throws SQLException{
        PreparedStatement sentencia = null;
        java.sql.Date fechaSQL = new java.sql.Date(System.currentTimeMillis());

        conexion.setAutoCommit(false);

        fechaSQL = new java.sql.Date(empresa.getFecha_fundacion().getTime());

        sentencia = conexion.prepareStatement("UPDATE empresas SET nombre = ?, descripcion = ?, codigo = ?, activos = ?, fecha_fundacion = ? WHERE id = ?");
        sentencia.setString(1, empresa.getNombre());
        sentencia.setString(2, empresa.getDescripcion());
        sentencia.setInt(3, empresa.getCodigo());
        sentencia.setFloat(4, empresa.getActivos());
        sentencia.setDate(5, fechaSQL);
        sentencia.setInt(6, empresa.getId());
        sentencia.executeUpdate();
        conexion.commit();

        if (sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);
    }

    public void modificarAutobus (Autobus autobus) throws SQLException{
        PreparedStatement sentencia = null;
        java.sql.Date fechaSQL = new java.sql.Date(System.currentTimeMillis());

        conexion.setAutoCommit(false);

        fechaSQL = new java.sql.Date(autobus.getFecha_compra().getTime());

        sentencia = conexion.prepareStatement("UPDATE autobuses SET nombre = ?, descripcion = ?, matricula = ?, consumo = ?, fecha_compra = ?, id_empresa = ? WHERE id = ?");
        sentencia.setString(1, autobus.getNombre());
        sentencia.setString(2, autobus.getDescripcion());
        sentencia.setInt(3, autobus.getMatricula());
        sentencia.setFloat(4, autobus.getConsumo());
        sentencia.setDate(5, fechaSQL);
        sentencia.setInt(6, autobus.getEmpresa().getId());
        sentencia.setInt(7, autobus.getId());
        sentencia.executeUpdate();
        conexion.commit();

        if (sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);
    }

    public void modificarConductor (Conductor conductor) throws SQLException{
        PreparedStatement sentencia = null;
        java.sql.Date fechaSQL = new java.sql.Date(System.currentTimeMillis());

        conexion.setAutoCommit(false);

        fechaSQL = new java.sql.Date(conductor.getFecha_nacimiento().getTime());

        sentencia = conexion.prepareStatement("UPDATE conductores SET nombre = ?, apellido = ?, fecha_nacimiento = ?, jornada = ?, salario = ?, id_empresa = ?, id_autobus = ? WHERE id = ?;");
        sentencia.setString(1, conductor.getNombre());
        sentencia.setString(2, conductor.getApellido());
        sentencia.setDate(3, fechaSQL);
        sentencia.setInt(4, conductor.getJornada());
        sentencia.setFloat(5, conductor.getSalario());
        sentencia.setInt(6, conductor.getEmpresa().getId());
        sentencia.setInt(7, conductor.getAutobus().getId());
        sentencia.setInt(8, conductor.getId());
        sentencia.executeUpdate();
        conexion.commit();

        if (sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);
    }

    public void eliminarEmpresa(Empresa empresa) throws  SQLException{
        PreparedStatement sentencia = null;

        conexion.setAutoCommit(false);

        sentencia = conexion.prepareStatement("DELETE FROM empresas WHERE id = '"+empresa.getId()+"';");
        sentencia.executeUpdate();
        conexion.commit();

        if (sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);
    }

    public void eliminarAutobus(Autobus autobus) throws SQLException{
        PreparedStatement sentencia = null;

        conexion.setAutoCommit(false);

        sentencia = conexion.prepareStatement("DELETE FROM autobuses WHERE id = '"+autobus.getId()+"';");
        sentencia.executeUpdate();
        conexion.commit();

        if(sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);
    }

    public void eliminarConductor(Conductor conductor) throws SQLException {
        PreparedStatement sentencia = null;

        conexion.setAutoCommit(false);

        sentencia = conexion.prepareStatement("DELETE FROM conductores WHERE id = '"+conductor.getId()+"';");
        sentencia.executeUpdate();
        conexion.commit();

        if(sentencia != null)
            sentencia.close();

        conexion.setAutoCommit(true);
    }

    public ArrayList<Autobus> getAutobusesEmpresa(Empresa empresa) throws SQLException{
        ArrayList<Autobus> listaBuses = new ArrayList<Autobus>();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT id FROM autobuses WHERE id_empresa = '"+empresa.getId()+"'");
        resultado = sentencia.executeQuery();

        while (resultado.next()) {
            listaBuses.add(getAutobus(resultado.getInt(1)));
        }

        if (sentencia != null)
            sentencia.close();

        return listaBuses;
    }

    public ArrayList<Conductor> getConductoresDeX(String tabla, Empresa empresa, Autobus autobus) throws SQLException{
        ArrayList<Conductor> listaConductores = new ArrayList<Conductor>();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        if(tabla.equals("empresa")){
            sentencia = conexion.prepareStatement("SELECT id FROM conductores WHERE id_empresa = '"+empresa.getId()+"'");
        }else if (tabla.equals("autobus")){
            sentencia = conexion.prepareStatement("SELECT id FROM conductores WHERE id_autobus = '"+autobus.getId()+"'");
        }

        resultado = sentencia.executeQuery();

        while (resultado.next()) {
            listaConductores.add(getConductor(resultado.getInt(1)));
        }

        if (sentencia != null)
            sentencia.close();

        return listaConductores;
    }

    public Empresa buscarEmpresa(String nombre) throws SQLException {
        Empresa empresa = new Empresa();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM empresas WHERE nombre = '"+nombre+"';");
        resultado = sentencia.executeQuery();

        resultado.first();

        empresa.setId(resultado.getInt(1));
        empresa.setNombre(resultado.getString(2));
        empresa.setDescripcion(resultado.getString(3));
        empresa.setCodigo(resultado.getInt(4));
        empresa.setActivos(resultado.getFloat(5));
        empresa.setFecha_fundacion(resultado.getDate(6));

        if (sentencia != null)
            sentencia.close();

        return empresa;
    }

    public Autobus buscarAutobus (String nombre) throws SQLException{
        Autobus autobus = new Autobus();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM autobuses WHERE nombre = '"+nombre+"';");
        resultado = sentencia.executeQuery();

        resultado.first();

        autobus.setId(resultado.getInt(1));
        autobus.setNombre(resultado.getString(2));
        autobus.setDescripcion(resultado.getString(3));
        autobus.setMatricula(resultado.getInt(4));
        autobus.setConsumo(resultado.getFloat(5));
        autobus.setFecha_compra(resultado.getDate(6));
        autobus.setEmpresa(getEmpresa(resultado.getInt(7)));

        if (sentencia != null)
            sentencia.close();

        return autobus;
    }

    public Conductor buscarConductor (String nombre) throws SQLException{
        Conductor conductor = new Conductor();
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement("SELECT * FROM conductores WHERE nombre = '"+nombre+"';");
        resultado = sentencia.executeQuery();

        resultado.first();

        conductor.setId(resultado.getInt(1));
        conductor.setNombre(resultado.getString(2));
        conductor.setApellido(resultado.getString(3));
        conductor.setFecha_nacimiento(resultado.getDate(4));
        conductor.setJornada(resultado.getInt(5));
        conductor.setSalario(resultado.getFloat(6));
        conductor.setEmpresa(getEmpresa(resultado.getInt(7)));
        conductor.setAutobus(getAutobus(resultado.getInt(8)));

        if (sentencia != null)
            sentencia.close();

        return conductor;
    }
}
