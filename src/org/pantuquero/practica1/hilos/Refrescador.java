package org.pantuquero.practica1.hilos;

import org.pantuquero.practica1.gui.Ventana;

import java.sql.Connection;
import java.sql.SQLException;

public class Refrescador extends Thread {
    private Connection conexion;
    private Ventana ventana;

    public Refrescador(Ventana ventana, Connection conexion){
        this.ventana = ventana;
        this.conexion = conexion;
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(5000);
                if(!conexion.isClosed()){
                    ventana.cargarTablas();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
