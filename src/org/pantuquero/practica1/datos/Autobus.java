package org.pantuquero.practica1.datos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

/**
 * Created by Pantuquero on 12/11/2014.
 *
 * Clase Autobús que contiene los org.pantuquero.practica1.datos de los autobuses (nombre, descripcion, matrícula, consumo, fecha de compra,
 * la empresa a la que pertenece y los conductores que lo utilizan).
 * @see
 */
public class Autobus implements Serializable{
    private int id;
    private String nombre;
    private String descripcion;
    private int matricula;
    private float consumo;
    private Date fecha_compra;

    private Empresa empresa;
    private ArrayList<Conductor> conductores;

    /**
     * Al crear la clase el constructor rellena las cadenas con información vacía, los números con 0 y la date con
     * la fecha actual.
     */
    public Autobus(){
        this.id = 0;
        this.nombre = "";
        this.matricula = 0;
        this.consumo = 0;
        this.fecha_compra = new Date();
        this.descripcion = "";
        this.empresa = new Empresa();
        this.conductores = new ArrayList<Conductor>();
    }

    @Override
    public String toString() {
        return String.valueOf(this.matricula);
    }

    public void addConductor(Conductor conductor){
        this.conductores.add(conductor);
    }

    public void removeConductor(Conductor conductor){
        this.conductores.remove(conductor);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }

    public Date getFecha_compra() {
        return fecha_compra;
    }

    public void setFecha_compra(Date fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public ArrayList<Conductor> getConductores() {
        return conductores;
    }

    public void setConductores(ArrayList<Conductor> conductores) {
        this.conductores = conductores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
