package org.pantuquero.practica1.datos;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Pantuquero on 12/11/2014.
 *
 * Clase conductor que contiene los org.pantuquero.practica1.datos de los conductores (nombre, apellido, fecha de nacimiento, jornada, salario,
 * la empresa a la que pertenece y el autobús que conduce).
 */

/**
 * Al crear la clase el constructor rellena las cadenas con información vacía, los números con 0 y la date con
 * la fecha actual.
 */
public class Conductor implements Serializable{
    private int id;
    private String nombre;
    private String apellido;
    private Date fecha_nacimiento;
    private int jornada;
    private float salario;

    private Empresa empresa;
    private Autobus autobus;

    public void Conductor(){
        this.id = 0;
        this.jornada = 0;
        this.nombre = "";
        this.apellido = "";
        this.fecha_nacimiento = new Date();
        this.salario = 0;
        this.empresa = new Empresa();
        this.autobus = new Autobus();
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public int getJornada() {
        return jornada;
    }

    public void setJornada(int jornada) {
        this.jornada = jornada;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Autobus getAutobus() {
        return autobus;
    }

    public void setAutobus(Autobus autobus) {
        this.autobus = autobus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
