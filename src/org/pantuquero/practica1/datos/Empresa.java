package org.pantuquero.practica1.datos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

/**
 * Created by Pantuquero on 12/11/2014.
 *
 * Clase empresa que contiene los org.pantuquero.practica1.datos de las empresas (nombre, descripción, código, activos, fecha de fundación,
 * los autobuses que posee y los conductores que trabajan en ella).
 */
public class Empresa implements Serializable{
    private int id;
    private String nombre;
    private String descripcion;
    private int codigo;
    private float activos;
    private Date fecha_fundacion;

    private ArrayList<Autobus> autobuses = new ArrayList<Autobus>();
    private ArrayList<Conductor> conductores = new ArrayList<Conductor>();

    /**
     * Al crear la clase el constructor rellena las cadenas con información vacía, los números con 0 y la date con
     * la fecha actual.
     */
    public void Empresa(){
        this.id = 0;
        this.nombre = "";
        this.descripcion = "";
        this.codigo = 0;
        this.activos = 0;
        this.fecha_fundacion = new Date();
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    public void addAutobus(Autobus autobus){
        this.autobuses.add(autobus);
    }

    public void addConductor(Conductor conductor){
        this.conductores.add(conductor);
    }

    public void removeAutobus(Autobus autobus){
        this.autobuses.remove(autobus);
    }

    public void removeConductor(Conductor conductor){
        this.conductores.remove(conductor);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public float getActivos() {
        return activos;
    }

    public void setActivos(float activos) {
        this.activos = activos;
    }

    public Date getFecha_fundacion() {
        return fecha_fundacion;
    }

    public void setFecha_fundacion(Date fecha_fundacion) {
        this.fecha_fundacion = fecha_fundacion;
    }

    public ArrayList<Autobus> getAutobuses() {
        return autobuses;
    }

    public void setAutobuses(ArrayList<Autobus> autobuses) {
        this.autobuses = autobuses;
    }

    public ArrayList<Conductor> getConductores() {
        return conductores;
    }

    public void setConductores(ArrayList<Conductor> conductores) {
        this.conductores = conductores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
