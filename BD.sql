CREATE DATABASE IF NOT EXISTS pr2ad;

USE pr2ad;

CREATE TABLE IF NOT EXISTS empresas(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(20),
	descripcion VARCHAR(500),
	codigo INT,
	activos FLOAT,
	fecha_fundacion DATE
);

CREATE TABLE IF NOT EXISTS autobuses (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(20),
	descripcion VARCHAR(500),
	matricula INT(4) UNSIGNED,
	consumo FLOAT,
	fecha_compra DATE,
	id_empresa INT UNSIGNED,
	FOREIGN KEY (id_empresa) 
	REFERENCES empresas(id)
	ON DELETE SET NULL
	ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS conductores(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(20),
	apellido VARCHAR(20),
	fecha_nacimiento DATE,
	jornada INT(2),
	salario FLOAT,
	id_empresa INT UNSIGNED,
	id_autobus INT UNSIGNED,
	FOREIGN KEY (id_empresa) 
	REFERENCES empresas(id)
	ON DELETE SET NULL
	ON UPDATE CASCADE,
	FOREIGN KEY (id_autobus) 
	REFERENCES autobuses(id)
	ON DELETE SET NULL
	ON UPDATE CASCADE
);